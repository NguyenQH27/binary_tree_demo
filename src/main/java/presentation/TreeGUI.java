package presentation;

import com.kenfogel.binarytree.implementation.BinaryTree;
import java.io.IOException;
import javafx.beans.InvalidationListener;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;
import javafx.stage.Stage;

/**
 *  Sets up the layout and event handlers for buttons
 * @author Huan
 */
public class TreeGUI {
 
    private BinaryTree tree;
    
    private Circle[][] circles;

    private String treeText;
    
    
    /**
     * constructor to initialize game
     * @throws IOException 
     */
    public TreeGUI(){
        initialize();
    }
    
    /**
     * Starts the game with a graphical user interface
     * @param primaryStage 
     */
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Binary Tree");
        VBox vb = new VBox(5);
        GridPane gp = createBoard();
        vb.getChildren().add(gp);
        
        Scene scene = new Scene(vb, 750, 750);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    /**
     * Creates the game board
     * @return 
     */
    private GridPane createBoard() {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(0);
        grid.setVgap(0);
        grid.setPadding(new Insets(25, 25, 25, 25));
        
        /*Circle circle1 = new Circle();
        Text text = new Text("123");
        text.setFont(new Font(30));
        text.setBoundsType(TextBoundsType.VISUAL); 
        StackPane stack = new StackPane();
        stack.getChildren().addAll(circle1, text);
        stack.setPadding(new Insets(20));
        circle1.setRadius(37.0);
        circle1.setFill(Color.ALICEBLUE);
        grid.add(stack, 0, 0);*/
        
        String[] lines = treeText.split("\n");
        
        int row = 0;
        for (String line : lines) {
            Text text = new Text(line);
            text.setFont(new Font(10));
            grid.add(text, 0, row);
            row++;
        }

        
        return grid;
    }
    
    /**
     * Initializes the gui nodes
     */
    private void initialize() {
        circles = new Circle[2][2];
        tree = new BinaryTree<>();
        int[] data = {400, 200, 800, 900, 111, 300, 700, 500, 600, 100, 401, 400, 9999, 55555};
        for (int number : data) {
            tree.insert(number);
        }
        treeText = tree.getTree();
    }
    
    /**
     * Shows a pop up with a given title
     * @param title 
     */
    private void alertMsg(String title) {
        Stage dialog = new Stage();
        VBox dialogVbox = new VBox(20);
        dialogVbox.getChildren().add(new Text(title));
        Scene dialogScene = new Scene(dialogVbox, 200, 100);
        dialog.setScene(dialogScene);
        dialog.show();
    }
    
}
