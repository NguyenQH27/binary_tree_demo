/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kenfogel.binarytree;

import javafx.application.Application;
import javafx.stage.Stage;
import presentation.TreeGUI;

/**
 *
 * @author Huan
 */
public class MainApp extends Application {
    
    private TreeGUI gui;
    
    @Override
    public void init() {
        gui = new TreeGUI();
    }
    
    @Override
    public void start(Stage primaryStage) {
        gui.start(primaryStage);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Application.launch(args);
    }
    
}
