package com.kenfogel.binarytree.implementation;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Queue;

/**
 * Based on the code found at http://cslibrary.stanford.edu/110/BinaryTrees.html
 *
 * @author Ken Fogel
 * @param <T>
 */
public class BinaryTree<T extends Comparable<T>> {

    // Root node reference. Will be null for an empty tree.
    private BinaryTreeNode<T> root;
    

    /**
     * Creates an empty binary tree -- a null root reference.
     */
    public BinaryTree() {
        root = null;
    }

    /**
     * Inserts the given data into the binary tree.Uses a recursive helper.
     *
     * @param data
     */
    public void insert(T data) {
        root = insert(root, data);
    }

    private BinaryTreeNode<T> insert(BinaryTreeNode<T> node, T data) {
        if (node == null) {
            node = new BinaryTreeNode(data);
        } else {
            if (data.compareTo(node.data) <= 0) {
                node.left = insert(node.left, data);
            } else {
                node.right = insert(node.right, data);
            }
        }

        return (node); // in any case, return the new reference to the caller
    }

    /**
     * Returns true if the given target is in the binary tree.Uses a recursive
     * helper.
     *
     * @param data
     * @return true of false depending on whether the data is found
     */
    public boolean lookup(T data) {
        return (lookup(root, data));
    }

    /**
     * Recursive lookup -- given a node, recur down searching for the given
     * data.
     */
    private boolean lookup(BinaryTreeNode<T> node, T data) {
        if (node == null) {
            return (false);
        }

        if (data == node.data) {
            return (true);
        } else if (data.compareTo(node.data) < 0) {
            return (lookup(node.left, data));
        } else {
            return (lookup(node.right, data));
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    /**
     * Returns the number of nodes in the tree. Uses a recursive helper that
     * recurses down the tree and counts the nodes.
     *
     * @return the number of elements in the tree
     */
    public int size() {
        return (size(root));
    }

    private int size(BinaryTreeNode<T> node) {
        if (node == null) {
            return (0);
        } else {
            return (size(node.left) + 1 + size(node.right));
        }
    }

    /**
     * Returns the max root-to-leaf depth of the tree. Uses a recursive helper
     * that recurses down to find the max depth.
     *
     * @return The depth of the tree from the root to the lowest node
     */
    public int maxDepth() {
        return (maxDepth(root));
    }

    private int maxDepth(BinaryTreeNode<T> node) {
        if (node == null) {
            return (0);
        } else {
            int lDepth = maxDepth(node.left);
            int rDepth = maxDepth(node.right);

            // use the larger + 1 
            return (Math.max(lDepth, rDepth) + 1);
        }
    }

    /**
     * Returns the min value in a non-empty binary search tree. Uses a helper
     * method that iterates to the left to find the min value.
     *
     * @return The smallest value in the tree
     */
    public T minValue() {
        return (minValue(root));
    }

    /**
     * Finds the min value in a non-empty binary search tree.
     */
    private T minValue(BinaryTreeNode<T> node) {
        BinaryTreeNode<T> current = node;
        while (current.left != null) {
            current = current.left;
        }

        return (current.data);
    }

    /**
     * Prints the node values in the "inorder" order. Uses a recursive helper to
     * do the traversal.
     */
    public void printInorderTree() {
        printInorderTree(root);
        System.out.println();
    }

    private void printInorderTree(BinaryTreeNode<T> node) {
        if (node == null) {
            return;
        }

        // left, node itself, right 
        printInorderTree(node.left);
        System.out.print(node.data + "  ");
        printInorderTree(node.right);
    }

    /**
     * Prints the node values in the "postorder" order. Uses a recursive helper
     * to do the traversal.
     */
    public void printPostorder() {
        printPostorder(root);
        System.out.println();
    }

    private void printPostorder(BinaryTreeNode<T> node) {
        if (node == null) {
            return;
        }

        // first recur on both subtrees 
        printPostorder(node.left);
        printPostorder(node.right);

        // then deal with the node 
        System.out.print(node.data + "  ");
    }

    /**
     * Given a binary tree, prints out all of its root-to-leaf paths, one per
     * line. Uses a recursive helper to do the work.
     */
    public void printPaths() {
        List<T> path = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            path.add(null);
        }
        printPaths(root, path, 0);
    }

    /**
     * Recursive printPaths helper -- given a node, and an array containing the
     * path from the root node up to but not including this node, prints out all
     * the root-leaf paths.
     */
    private void printPaths(BinaryTreeNode<T> node, List<T> path, int pathLen) {
        if (node == null) {
            return;
        }
        
        // append this node to the path array 
        path.set(pathLen, node.data);
        pathLen++;

        // it's a leaf, so print the path that led to here 
        if (node.left == null && node.right == null) {
            printArray(path, pathLen);
        } else {
            // otherwise try both subtrees
            printPaths(node.left, path, pathLen);
            printPaths(node.right, path, pathLen);
        }
    }

    /**
     * Utility that prints Ts from an array on one line.
     */
    private void printArray(List<T> ts, int len) {
        int i;
        for (i = 0; i < len; i++) {
            System.out.print(ts.get(i) + " ");
        }
        System.out.println();
    }

    /**
     * Prints out each level in the tree from left to right. Uses a recursive
     * helper to do the work.
     */
    public void printLineByLine() {
        printLineByLine(root);
    }

    private void printLineByLine(BinaryTreeNode root) {
        if (root == null) {
            return;
        }
        final Queue<BinaryTreeNode> queue = new ArrayDeque<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            final int size = queue.size();
            for (int i = 0; i < size; i++) {
                BinaryTreeNode x = queue.remove();
                System.out.print(x.data + " ");
                if (x.left != null) {
                    queue.add(x.left);
                }
                if (x.right != null) {
                    queue.add(x.right);
                }
            }
            // new level
            System.out.println();
        }
    }

    
    public void printTree() {
        System.out.println(buildTree(Collections.singletonList(root), 1, maxDepth()));
    }
    
    public String getTree() {
        return buildTree(Collections.singletonList(root), 1, maxDepth());
    }
    
    /**
     * Based on https://stackoverflow.com/a/4973083
     * 
     * Recursive method to build a binary tree, where each node can only be 
     * at max 3 characters long. If it is more than that, then the rest is cut off and
     * replaced by '.' ex: '123123' -> '123.'
     * 
     * @param nodes
     * @param level
     * @param maxLevel
     */
    private String buildTree(List<BinaryTreeNode<T>> nodes, int level, int maxLevel){
        
        String result = "";
        
        //end recursive call when there are no more nodes
        if (nodes.isEmpty() || isAllElementsNull(nodes)) {
            return result;
        }
        
        //+1 to add a level because otherwise the last nodes would get too close to eachother
        int currentLevel = maxLevel - level + 1;
        
        //number of lines in a level (i.e. the lines with '/' and '\')
        int levelLines = (int) Math.pow(2, currentLevel - 1);
        
        //the beginning spaces of a line
        int leadingSpaces = (int) Math.pow(2, currentLevel) - 1; 
        
        //the horizontal space between 2 nodes
        int spacesBetween = (int) Math.pow(2, currentLevel + 1) - 1;
        
        result += printWhitespaces(leadingSpaces - 1);
        
        //list to hold the next nodes which will be passed on the next recursive call
        List<BinaryTreeNode<T>> nextNodes = new ArrayList<>();
        
        for (int i = 0; i < nodes.size(); i++) {
            
            if (nodes.get(i) == null) {
                //making its children null
                nextNodes.add(null);
                nextNodes.add(null);
                result += "   ";
                result += printWhitespaces(spacesBetween - 2);
            } else {
                nextNodes.add(nodes.get(i).left);
                nextNodes.add(nodes.get(i).right);
                
                String displayData = formatData(nodes.get(i).data.toString());
                
                result += displayData;
                
                //if last node in line, skip to next line
                if (i == nodes.size() - 1) {
                    continue;
                }
                
                result += printWhitespaces(spacesBetween - displayData.length() + 1);
                
            }
        }
        result += "\n";
        
        for (int i = 1; i <= levelLines; i++) {
            
            for (int j = 0; j < nodes.size(); j++) {
                //the spaces before each '/'
                result += printWhitespaces(leadingSpaces - i);
                
                if (nodes.get(j) == null) {
                    //no node so we print out spaces
                    result += printWhitespaces(levelLines + levelLines + i + 1); //+1 is for the missing '/'
                    
                    //skip all the other printing that would happen if there was a node
                    continue;
                }
                
                if (nodes.get(j).left != null) {
                    result += "/";
                } else {
                    result += " ";
                }
                //printing the spaces between the '/' and '\'
                result += printWhitespaces(i + i - 1);
                
                if (nodes.get(j).right != null) {
                    result += "\\";
                } else {
                    result += " ";
                }
                
                if (j == nodes.size() - 1) {
                    continue;
                }
                
                //printing spaces after the '\'
                result += printWhitespaces(levelLines + levelLines - i);
            }
            result += "\n";
        }
        return result + buildTree(nextNodes, level + 1, maxLevel);
    }
    
    private String formatData(String data) {
        
        if (data.length() > 3) {
            return data.substring(0, 3) + ".";
        } else if (data.length() == 2) {
            return data + " ";
        } else if (data.length() == 1) {
            return " " + data + " ";
        } else {
            return data;
        }
    }
    
    /**
     * Prints out the space character the number of times specified
     * @param count 
     */
    private String printWhitespaces(int count) {
        String result = "";
        for (int i = 0; i < count; i++) {
            result += " ";
        }
        return result;
    }
    
    /**
     * Checks if all the nodes in a tree level are null
     * @param list
     * @return 
     */
    private boolean isAllElementsNull(List<BinaryTreeNode<T>> list) {
        for (BinaryTreeNode<T> node : list) {
            if (node != null)
                return false;
        }
        return true;
    }
}